﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGal.Register;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        //  ALLOWS US TO AUTENTICATE USER
         private UserManager<Seller> _userManager;
        //  private ApplicContext _dbcontext;

        // private SignInManager<User> _signInManager; //  for coocie

         public AccountController(UserManager<Seller> userManager)//SignInManager<User> signInManager)
           {
        _userManager = userManager;
        // _signInManager = signInManager;
         }
       
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {

            var user = new Seller
            {

                Email = model.Email,
                UserName = model.Name
              
                
                //Year = model.Year
            };
            var data = await _userManager.CreateAsync(user, model.Password);
            return Ok(data);
        }
    }
}
