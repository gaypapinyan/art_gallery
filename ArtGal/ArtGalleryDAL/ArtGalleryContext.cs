﻿using ArtGalleryDAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL
{
    public class ArtGalleryContext : IdentityDbContext
    {

        public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) : base(options)
        {

            //  Database.Migrate();
        }

        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ArtObject> Objects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           /* modelBuilder.Entity<Seller>(b =>
            {
                b.HasKey(u => u.Id);
                b.HasIndex(u => u.NormalizedUserName).HasName("UserNameIndex").IsUnique();
                b.HasIndex(u => u.NormalizedEmail).HasName("EmailIndex");
                b.ToTable("AspNetUsers");
                b.Property(u => u.ConcurrencyStamp).IsConcurrencyToken();

                b.Property(u => u.UserName).HasMaxLength(256);
                b.Property(u => u.NormalizedUserName).HasMaxLength(256);
                b.Property(u => u.Email).HasMaxLength(256);
                b.Property(u => u.NormalizedEmail).HasMaxLength(256);

                // Replace with b.HasMany<IdentityUserClaim>().
                b.HasMany<IdentityUserClaim<long>>().WithOne().HasForeignKey(uc => uc.UserId).IsRequired();
                b.HasMany<IdentityUserLogin<long>>().WithOne().HasForeignKey(ul => ul.UserId).IsRequired();
                b.HasMany<IdentityUserToken<long>>().WithOne().HasForeignKey(ut => ut.UserId).IsRequired();
            });*/
            // modelBuilder.Entity<Seller>()
            //      .HasKey(p => p.Id);
            //modelBuilder.Entity<Seller>()
            //   .Property(f => f.Id)
            //   .ValueGeneratedOnAdd();
            modelBuilder.Entity<IdentityUserLogin<string>>().HasNoKey();
            modelBuilder.Entity<IdentityUserRole<string>>().HasNoKey();
            modelBuilder.Entity<IdentityUserToken<string>>().HasNoKey();

            modelBuilder.Entity<Buyer>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<Buyer>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<ArtObject>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<ArtObject>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ArtObject>()
               .HasOne(t => t.seller)
               .WithMany(p => p.artObjects)
               .HasForeignKey(p => p.sellerId)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ArtObject>()
                .Property(p => p.sellerId);

            modelBuilder.Entity<ShoppingCart>()
               .HasKey(p => p.Id);
            modelBuilder.Entity<ShoppingCart>()
               .Property(f => f.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<ShoppingCart>()
               .HasOne(p => p.artObject)
               .WithMany(p => p.shoppingCart)
               .HasForeignKey(p => p.artObjectId)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ShoppingCart>()
               .HasOne(p => p.buyer)
               .WithMany(p => p.shoppingCart)
               .HasForeignKey(p => p.buyerId)
               .OnDelete(DeleteBehavior.Cascade);

            //modelBuilder.SeedData();
        }
    }
}
