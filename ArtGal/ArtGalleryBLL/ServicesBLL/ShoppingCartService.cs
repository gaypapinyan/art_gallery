﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.ServicesBLL
{
    public class ShoppingCartService : ServiceBase<ShoppingCart, ShoppingCartDTO>, IShoppingCartService
    {


        public ShoppingCartService(IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {

        }

        public IArtObjectService artObjectService { get; set; }

        public async Task<ShoppingCartDTO> BuyObject(ShoppingCartDTO model)
        {
            //var data = mapper.Map<ShoppingCartItem>(model);
            ///poxy poxancec
            var obj = await artObjectService.GetById(model.artObjectId);
            //obj.UserId = model.buyerId;
            await artObjectService.Update(obj);
            var result = await Remove(model.Id);
            return result;
        }

    }
}
