﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [Route("api/buyer")]
    [ApiController]
    public class BuyerController : ControllerBase
    {
        private IBuyerService buyerService;

        public BuyerController(IBuyerService buyerService)
        {
            this.buyerService = buyerService;
        }

        // GET: api/buyer
        //[Route("~/api/Get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = await buyerService.GetAll();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        // GET: api/buyer/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {

            try
            {
                var data = await buyerService.GetById(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        // POST: api/buyer
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BuyerDTO artObject)
        {
            try
            {
                var data = await buyerService.Add(artObject);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        // PUT: api/buyer/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] BuyerDTO buyer)
        {
            if (id == buyer.Id)
            {
                buyerService.Update(buyer);
                return Ok("Updated successfully!");
            }
            return BadRequest("Error! Couldn't update");
        }

        // DELETE: api/buyer/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await buyerService.GetById(id);
            if (data == null)
            {
                return BadRequest(id);
            }
            else
            {
                await buyerService.Remove(id);
                return Ok("Removed successfully!");
            }

        }
    }
}
