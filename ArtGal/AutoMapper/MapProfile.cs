﻿using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AutoMapper
{
    public static class AutoMapper
    {
        public static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapProfile));
            return services;
        }
    }
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Buyer, BuyerDTO>()
                .ReverseMap();

            CreateMap<Seller, SellerDTO>()
            .ReverseMap();

            CreateMap<ShoppingCart, ShoppingCartDTO>()
                //.ForMember(p => p.buyerId, opt => opt.MapFrom(src => src.buyer.Id))
                //.ForMember(p => p.artObjectId, opt => opt.MapFrom(src => src.artObject.Id))
                .ReverseMap()
                .ForPath(s => s.buyer, src => src.Ignore())
                .ForPath(s => s.artObject, src => src.Ignore());

            CreateMap<ArtObject, ArtObjectDTO>()
              //.ForMember(s => s.sellerId, opt => opt.MapFrom(src => src.seller.Id))
              .ReverseMap()
              .ForPath(s => s.seller, opt => opt.Ignore());

        }
    }
}
