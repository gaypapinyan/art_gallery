﻿using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface ISellerService : IBaseService<Seller, SellerDTO>
    {

    }
}
