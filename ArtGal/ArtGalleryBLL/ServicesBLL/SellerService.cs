﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.ServicesBLL
{
    public class SellerService : ServiceBase<Seller, SellerDTO>, ISellerService
    {
        public SellerService(IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {

        }

    }
}
